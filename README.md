# CRUD with Postgres and python (:

A book store crud, enjoy!

## If error running psycopg2:

## try:

```
sudo apt install libpq-dev python3-dev
```

and

```
pip install psycopg2
```

## If that doesn't work...

```
sudo apt install build-essential
```

and

```
sudo apt install postgresql-server-dev-all
```

and again:

```
pip install psycopg2
```

## If error role '<>' doesn't exists:

* Go to role_error_script.py and change the name of the user and database


## HOW TO RUN THE PROJECT

## On venv:
* 'LOCAL':
```
docker pull postgres:10.8
```

```
docker run -d -v PGDATA:/tmp -p 54320:5432 postgres:10.8
```

and

```
python3 debug.py
```

* 'DOCKER':
```
docker-compose build
```

```
docker-compose up
```

## Tests:

Run(be sure docker is running with your db):

```
python3 -m unittest __tests__/system_test.py -v
```

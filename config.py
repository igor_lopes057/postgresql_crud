import os


basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    DEBUG = False
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = None

class LocalConfig(Config):
    DEBUG = True
    TESTING = True
    DATABASE_URL = 'postgresql+psycopg2://127.0.0.1:54320/books_store_local'
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://127.0.0.1:54320/books_store_local'

class DockerConfig(Config):
    DEBUG = False
    DATABASE_URL = 'postgresql+psycopg2://db:5432/books_store'
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://db:5432/books_store'


config = {
    'default': Config,
    'docker': DockerConfig,
    'local': LocalConfig
}

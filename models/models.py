from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Books(Base):
    __tablename__ = 'books_store'
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String(300), nullable=False)
    author = Column(String(300), nullable=False)
    year = Column(String(100), nullable=False)

    def __init__(self, title=None, author=None, year=None):
        self.title = title
        self.author = author
        self.year = year

    def serialize(self):
        return {
            'title': self.title,
            'author': self.author,
            'year': self.year,
        }

    def __repr__(self):
        return 'Livro \"{}\" escrito por \"{}\" em {}.'.format(self.title,
                                                               self.author,
                                                               self.year)

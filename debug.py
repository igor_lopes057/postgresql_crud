from app.application import run_app
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from config import config
import os


if __name__ == '__main__':
    os.environ['FLASK_ENV'] = 'LOCAL'
    app = run_app()
    app.config.from_object(config['local'])
    app.run(debug=True, host='0.0.0.0', port=5000)

from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import psycopg2


def role_error():
    conn = psycopg2.connect("host='127.0.0.1' port='54320' user='postgres'")
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cur = conn.cursor()
    try:
        cur.execute("CREATE ROLE igor_montilha WITH LOGIN PASSWORD 'password';")
        cur.execute("CREATE DATABASE \"books_store_local\" OWNER = igor_montilha;")
        cur.execute("GRANT ALL PRIVILEGES ON DATABASE \"books_store_local\" TO igor_montilha;")
        conn.commit()
    except:
        cur.close()
        conn.close()

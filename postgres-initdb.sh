#!/bin/sh -e

psql --variable=ON_ERROR_STOP=1 --username "postgres" <<-EOSQL
    CREATE ROLE root WITH LOGIN PASSWORD 'password';
    CREATE DATABASE "books_store" OWNER = root;
    GRANT ALL PRIVILEGES ON DATABASE "books_store" TO root;
EOSQL

from flask import url_for


def get_data(title: str, author: str, year: str):
    data = {
        'title': title,
        'author': author,
        'year': year
    }
    return data


def create_data(client, func):
    data = func
    client.post(url_for('adding'), data=data)
    return data


def update_data(func, new_title: str, new_author: str, new_year: str):
    data = func
    data['old_title'] = data.pop('title')
    data['old_author'] = data.pop('author')
    data['old_year'] = data.pop('year')
    data.update({
        'new_title': new_title,
        'new_author': new_author,
        'new_year': new_year
    })
    return data


def delete_data(func):
    data = func
    data['delete_title'] = data.pop('title')
    data['delete_author'] = data.pop('author')
    data['delete_year'] = data.pop('year')
    return data

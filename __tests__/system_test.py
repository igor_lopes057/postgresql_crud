from unittest import TestCase, main
from flask import url_for
from sqlalchemy.orm import sessionmaker
from __tests__.helpers import get_data, create_data, delete_data, update_data
from app.application import set_routes
from models.models import Books, Base
from sqlalchemy import create_engine
from __init__ import create_app
from role_error_script import role_error
from config import config


engine = create_engine(
    'postgresql+psycopg2://127.0.0.1:54320/books_store_local'
)
role_error()
Session = sessionmaker(bind=engine, binds={})
session = Session()


class TestCRUDPostgresFlask(TestCase):
    @classmethod
    def setUpClass(cls):
        app = create_app()
        cls.app = app
        cls.app.config.from_object(config['local'])
        cls.app_context = cls.app.test_request_context()
        cls.app_context.push()
        cls.client = cls.app.test_client()
        cls.engine = engine
        Base.metadata.create_all(cls.engine)
        cls.session = session
        set_routes(cls.app, cls.session)

    @classmethod
    def tearDownClass(cls):
        cls.engine = engine
        Base.metadata.drop_all(cls.engine)

    def setUp(self):
        self.engine = engine
        self.connection = self.engine.connect()
        self.session = Session(bind=self.connection, binds={})

    def tearDown(self):
        self.session.query(Books).delete()
        self.session.commit()
        self.session.close()
        self.connection.close()

    def test__index__access_page__should_return_status_code_200_ok(self):
        # FIXTURE

        # EXECUTE
        response = self.client.get('/')

        # EXPECTED
        expected_response = 200
        self.assertEqual(expected_response, response.status_code)

    def test__adding__a_book_on_database__should_return_status_code_302_redirect(self):
        # FIXTURE

        # EXECUTE
        data = get_data(title='A Guerra dos Tronos. As Crônicas de Gelo e Fogo - Livro 1', author='George R. R. Martin',
                        year='1996')
        response = self.client.post(url_for('adding'), data=data)

        # EXPECTED
        expected_response = 302
        self.assertEqual(expected_response, response.status_code)

    def test__updating__a_book_on_database_with_title_in_blank__should_return_status_code_302_redirect(self):
        # FIXTURE
        data = create_data(client=self.client, func=get_data(title=' ', author='George R. R. Martin', year='1996'))

        # EXECUTE
        data = update_data(func=data, new_title='A Guerra dos Tronos. As Crônicas de Gelo e Fogo - Livro 1',
                           new_author='George R. R. Martin',
                           new_year='1996')
        response = self.client.post(url_for('updating'), data=data)

        # EXPECTED
        expected_response = 302
        self.assertEqual(expected_response, response.status_code)

    def test__deleting__a_book_from_database__should_return_status_code_302_redirect(self):
        # FIXTURE
        data = create_data(client=self.client, func=get_data(title='what', author='am I', year='doing'))

        # EXECUTE
        data = delete_data(func=data)
        response = self.client.post(url_for('deleting'), data=data)

        # EXPECTED
        expected_response = 302
        self.assertEqual(expected_response, response.status_code)

    def test__adding__a_value_on_database__should_return_value_found_correctly(self):
        # FIXTURE
        data = get_data(title='Study in pink', author='Arthur Conan Doyle',
                        year='27 de fevereiro de 1915')

        # EXECUTE
        create_data(client=self.client, func=data)
        query_value = str(
            self.session.query(Books).filter_by(title=data['title'], author=data['author'], year=data['year'])
            .first())

        # EXPECTED
        expected_value = 'Livro \"{}\" escrito por \"{}\" em {}.'.format(data['title'], data['author'], data['year'])
        self.assertEqual(expected_value, query_value)

    def test__updating__a_value_on_database__should_return_value_found_correctly(self):
        # FIXTURE
        data = get_data(title='The Valley of Fear', author='Obama', year='27 de fevereiro de 1915')
        create_data(client=self.client, func=data)

        # EXECUTE
        data = update_data(func=data, new_title='The Valley of Fear', new_author='Arthur Conan Doyle', new_year='1915')
        self.client.post(url_for('updating'), data=data)
        query_value = str(
            self.session.query(Books).filter_by(title=data['new_title'], author=data['new_author'], year=data['new_year'])
                .first())

        # EXPECTED
        expected_value = 'Livro \"{}\" escrito por \"{}\" em {}.'.format(data['new_title'], data['new_author'], data['new_year'])
        self.assertEqual(expected_value, query_value)

    def test__deleting__a_value_on_database__should_return_query_count_equals_to_zero(self):
        # FIXTURE
        data = get_data(title='Assassin\'s creed revelations', author='Oliver Bowder', year='2013')
        create_data(client=self.client, func=data)

        # EXECUTE
        data = delete_data(func=data)
        self.client.post(url_for('deleting'), data=data)
        query_count = self.session.query(Books).count()

        # EXPECTED
        expected_value = 0
        self.assertEqual(expected_value, query_count)

    def test__get_data__helper__should_return_correctly(self):
        # FIXTURE

        # EXECUTE
        data = get_data(title='A Guerra dos Tronos. As Crônicas de Gelo e Fogo - Livro 1', author='George R. R. Martin',
                        year='1996')

        # EXPECTED
        expected_value = {
            'title': data['title'],
            'author': data['author'],
            'year': data['year']
        }
        self.assertEqual(expected_value, data)


if __name__ == '__main__':
    main()

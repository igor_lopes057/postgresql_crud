from flask import render_template, request, redirect
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models.models import Books, Base
from __init__ import create_app
from role_error_script import role_error
from config import config
import os


def run_app():
    app = create_app()
    if os.environ['FLASK_ENV'] == 'LOCAL':
        app.config.from_object(config['local'])
        engine = create_engine(app.config['DATABASE_URL'])
        role_error()
    if os.environ['FLASK_ENV'] == 'DOCKER':
        app.config.from_object(config['docker'])
        engine = create_engine(app.config['DATABASE_URL'])
    Session = sessionmaker(bind=engine)
    session_ = Session()
    Base.metadata.create_all(engine)
    set_routes(app, session_)
    return app


def set_routes(app, session):
    @app.route('/', methods=['GET'])
    def index():
        count = session.query(Books).count()
        data = session.query(Books).order_by(Books.title).all()
        return render_template('index.html', count=count, data=data)

    @app.route('/add', methods=['POST'])
    def adding():
        title = request.form['title']
        author = request.form['author']
        year = request.form['year']
        data = Books(title, author, year)
        session.add(data)
        session.commit()
        return redirect('/')

    @app.route('/update', methods=['POST'])
    def updating():
        old_title = request.form['old_title']
        old_author = request.form['old_author']
        old_year = request.form['old_year']
        old_book = session.query(Books).filter_by(title=old_title,
                                                  author=old_author,
                                                  year=old_year).first()
        session.delete(old_book)
        new_title = request.form['new_title']
        new_author = request.form['new_author']
        new_year = request.form['new_year']
        if new_title == '':
            new_title = old_title
        if new_author == '':
            new_author = old_author
        if new_year == '':
            new_year = old_year
        new_book = Books(new_title, new_author, new_year)
        session.add(new_book)
        session.commit()
        return redirect('/')

    @app.route('/delete', methods=['POST'])
    def deleting():
        title = request.form['delete_title']
        author = request.form['delete_author']
        year = request.form['delete_year']
        delete_book = session.query(Books).filter_by(title=title,
                                                     author=author,
                                                     year=year).first()
        session.delete(delete_book)
        session.commit()
        return redirect('/')

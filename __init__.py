from flask import Flask
from config import config


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(config['default'])
    return app
